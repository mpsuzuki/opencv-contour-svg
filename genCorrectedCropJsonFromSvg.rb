#!/usr/bin/env ruby1.9

require "json"
require "nokogiri"

require "libRect.rb"
require "getOpts.rb"
require "nokogiri-class.rb"

jsonCropAreas = Hash.new
Opts.args.each do |pathJson|
  STDERR.printf("*** load %s...", pathJson)
  fh = File::open(pathJson, "r")
  js = JSON.parse(fh.read)
  fh.close

  js["jsonCropAreas"].each do |hs|
    jbn = hs["basename"].split(".")[0..-2].join(".")
    if (!jsonCropAreas.include?(jbn))
      jsonCropAreas[jbn] = Array.new
    end

    jsonCropAreas[jbn] << Rect.new().from_hs(hs)
  end
  STDERR.printf("ok\n", pathJson)
end

jsonCropAreas.each do |jbn, crs|
  isModified = false

  STDERR.printf("*** read %s/%s.svg...", Opts.src_svg_dir, jbn)
  pathSvg = Opts.src_svg_dir + "/" + jbn + ".svg"
  fh = File::open(pathSvg, "r")
  svgDoc = Nokogiri::XML::Document.parse(fh.read)
  fh.close

  #
  # replace too-big rect.glyph including multiple glyphs by manually corrected rects.
  #
  svgDoc.css("rect").select{|rect| !rect.has_class("page-number") && !rect.has_class("line")}.each do |rectA|
    crA = Rect.new().setByXYWH(rectA["x"].to_i, rectA["y"].to_i, rectA["width"].to_i, rectA["height"].to_i)
    crs.select{|crFixed| crA.includesCenterOf(crFixed)}.each do |crFixed|
      isModified = true
      rectA.add_class("incorrect")
      rectF = Nokogiri::XML::Element.new("rect", svgDoc)
      rectF["x"] = crFixed.offsetX
      rectF["y"] = crFixed.offsetY
      rectF["width"] = crFixed.width
      rectF["height"] = crFixed.height
      rectF.add_class("glyph").add_class("corrected").add_class(crFixed.to_clsnm)
      rectA.before(rectF)
    end
  end

  #
  # mark small rects included by manually corrected rects as "fragmented"
  #
  svgDoc.css("rect").select{|rect| !rect.has_class("line") && !rect.has_class("corrected")}.each do |rectA|
    if (rectA.has_class("incorrect"))
      next
    else
      crA = Rect.new().setByXYWH(rectA["x"].to_i, rectA["y"].to_i, rectA["width"].to_i, rectA["height"].to_i)
      # crs.select{|crFixed| crFixed.covers(crA)}.each do |crFixed|
      crs.select{|crFixed| crO = crFixed.getOverlapWith(crA) ; crO != nil && crO .sizeOfArea > 0.7 * crA.sizeOfArea}.each do |crFixed|
        isModified = true
        if (svgDoc.css("rect." + crFixed.to_clsnm).length == 0)
          rectF = Nokogiri::XML::Element.new("rect", svgDoc)
          rectF["x"] = crFixed.offsetX
          rectF["y"] = crFixed.offsetY
          rectF["width"] = crFixed.width
          rectF["height"] = crFixed.height
          rectF.add_class("glyph").add_class("corrected").add_class(crFixed.to_clsnm)
          rectA.before(rectF)
        end
        rectA.add_class("incorrect").add_class("fragmented")
      end
    end
  end

#  #
#  # merge small non-glyph rects whose center is covered by other rect.glyph
#  #
#  svgDoc.css("rect").select{|rect| (rect.get_classes() & ["glyph", "line", "page-number", "incorrect", "fragmented"]).length == 0}.each do |rectA|
#    crA = Rect.new().setByXYWH(rectA["x"].to_i, rectA["y"].to_i, rectA["width"].to_i, rectA["height"].to_i)
#    svgDoc.css("rect.glyph").each do |rectG|
#      crG = Rect.new().setByXYWH(rectG["x"].to_i, rectG["y"].to_i, rectG["width"].to_i, rectG["height"].to_i)
#      if (crG.includesCenterOf(crA))
#        rectG["x"] = [crG.x1, crA.x1].min
#        rectG["y"] = [crG.y1, crA.y1].min
#        rectG["width"] = [crG.x2, crA.x2].max - rectG["x"].to_i
#        rectG["height"] = [crG.y2, crA.y2].max - rectG["y"].to_i
#      end
#    end
#  end

  if (isModified)
    STDERR.printf("write %s/%s.svg...", Opts.dst_svg_dir, jbn)
    of = File::open(Opts.dst_svg_dir + "/" + jbn + ".svg", "w+")
    of.write(svgDoc.to_xml)
    of.close 
  else
    STDERR.printf("no change, ")
  end
  STDERR.printf("ok\n")
end
