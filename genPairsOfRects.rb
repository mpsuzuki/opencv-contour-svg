#!/usr/bin/env ruby1.9

require "json"
require "getOpts.rb"
require "libRect.rb"
require "nokogiri"
require "nokogiri-class.rb"
require "RMagick"


# polyfil for Range class
class Range
  def size
    return (self.max - self.min)
  end
end

def genPerPageHashFromJson(pathJson)
  perPageHash = Hash.new
  STDERR.printf("*** parse %s...", pathJson)
  fh = File::open(pathJson, "r")
  JSON.parse(fh.read)["jsonCropAreas"].each do |hs|
    b = hs["basename"]
    perPageHash[b] = Array.new if (!perPageHash.include?(b))
    perPageHash[b] << Rect.new.from_hs(hs.dup)
  end
  fh.close
  STDERR.printf("ok\n")
  return perPageHash
end

glyphsPerPage = genPerPageHashFromJson(Opts.glyph_json)
pagenumsPerPage = genPerPageHashFromJson(Opts.pagenum_json)


def genLinedSvg(b, lines)
  svgDoc = Nokogiri::XML::Document.parse('<?xml-stylesheet type="text/css" href="ywzs.css" ?><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>')
  elmImg =  Nokogiri::XML::Element.new("image", svgDoc)
  svgDoc.root << elmImg
  elmImg["xlink:href"] = Opts.src_pic_dir + "/" + b
  ping = Magick::ImageList.new.ping(elmImg["xlink:href"])
  svgDoc.root["width"] = ping.columns
  svgDoc.root["height"] = ping.rows
  elmImg["width"] = ping.columns
  elmImg["height"] = ping.rows
  ping.destroy!

  lines.each do |pts|
    elmLine = Nokogiri::XML::Element.new("line", svgDoc)
    svgDoc.root << elmLine
    elmLine["x1"] = pts[0]
    elmLine["y1"] = pts[1]
    elmLine["x2"] = pts[2]
    elmLine["y2"] = pts[3]
  end

  oSvgPath = Opts.dst_svg_dir + "/" + File::basename(b).split(".")[0..-2].join(".") + ".svg"
  STDERR.printf("*** generate %s\n", oSvgPath)
  fh = File::open(oSvgPath, "w")
  fh.write(svgDoc.to_xml)
  fh.close
end

prevB = nil
glyphsPerPage.keys.sort.each do |b|
  STDERR.printf("*** check %s\n", b)
  genSvg = false
  lines = []
  glyphsPerPage[b].each do |crG|
    ix = crG.data["xIdx"].to_i
    crGs = glyphsPerPage[b].select{|crG2| crG2.data["xIdx"] == crG.data["xIdx"]}
    crPNs = pagenumsPerPage[b].select{|crPN| crPN.data["xIdx"] == crG.data["xIdx"]}

    crPN = nil
    if (crPNs.length == crGs.length)
      crPN = crPNs.select{|crPN| crPN.data["yIdx"] == crG.data["yIdx"]}.first
      crG.data["link"] = "by-index" 
    end
    if (crPN == nil)
      crPN = crPNs.select{|crPN| crG.coversYOf(crPN)}.sort_by{|crPN| crG.distant2From(crPN)}.first
      crG.data["link"] = "covered-nearest" 
    end
    if (crPN == nil)
      crPN = crPNs.select{|crPN| crG.hasOverlapYWith(crPN)}.sort_by{|crPN| crG.getOverlapYRange(crPN).size}.last
      crG.data["link"] = "y-overlap-height" 
    end
    if (crPN == nil)
      crPN = crPNs.sort_by{|crPN|
        if (crPN.averageY < crG.averageY)
          (crG.y1 - crPN.y2).abs
        else
          (crPN.y1 - crG.y2).abs
        end
      }.first
      crG.data["link"] = "no-overlap-nearest" 
    end

    if (crPN != nil)
      lines << [crG.averageX, crG.averageY, crPN.averageX, crPN.averageY]
    end

    if (!crPN)
      puts("") if (prevB != nil && prevB != b)
      printf("%s\tG:%s(%d,%02d) -> PN:none\n", b, crG.to_s, crG.data["xIdx"], crG.data["yIdx"].to_i)
      prevB = b
      genSvg = true
    elsif (!Opts.include?("quiet") || (crG.data["yIdx"] != crPN.data["yIdx"]))
      puts("") if (prevB != nil && prevB != b)
      printf("%s\tG:%s(%d,%02d) -> PN:%s(%d,%02d)\t%s\n", b, crG.to_s, crG.data["xIdx"], crG.data["yIdx"].to_i,
                                                             crPN.to_s, crPN.data["xIdx"], crPN.data["yIdx"].to_i,
                                                          crG.data["link"])
      prevB = b
      genSvg = true
    end
  end

  if (genSvg && Opts.include?("src-pic-dir") && Opts.include?("dst-svg-dir"))
    genLinedSvg(b, lines)
  end
end
