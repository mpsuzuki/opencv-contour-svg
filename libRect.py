import re
import copy
import math

class Rect:
  def __init__(self):
    self.offsetX = None
    self.offsetY = None
    self.width = None
    self.height = None
    self.data = {}
    return None

  def x1(self):
    return self.offsetX

  def y1(self):
    return self.offsetY

  def setX1(self, v):
    self.offsetX = v
    return self

  def setY1(self, v):
    self.offsetY = v
    return self

  def w(self):
    return self.width

  def h(self):
    return self.height

  def x2(self):
    return (self.offsetX + self.width)

  def y2(self):
    return (self.offsetY + self.height)

  def setX2(self, v):
    if self.width == None:
      self.width = v - self.offsetX
    else:
      self.offsetX = v - self.width
    return self

  def setY2(self, v):
    if self.height == None:
      self.height = v - self.offsetY
    else:
      self.offsetY = v - self.height
    return self

  def averageX(self):
    return self.offsetX + (0.5 * self.width)

  def averageY(self):
    return self.offsetY + (0.5 * self.height)

  def sizeOfArea(self):
    return (self.width * self.height)

  def setByX1Y1X2Y2(self, x1, y1, x2, y2):
    self.setX1(x1)
    self.setY1(y1)
    self.setX2(x2)
    self.setY2(y2)
    return self

  def setByX1Y1X2Y2Arr(self, arr):
    self.setByX1Y1X2Y2(arr[0], arr[1], arr[2], arr[3])
    return self

  def setByXYWH(self, x, y, w, h):
    self.offsetX = x
    self.offsetY = y
    self.width = w
    self.height = h
    return self

  def setByXYWHArr(self, arr):
    self.setByXYWH(arr[0], arr[1], arr[2], arr[3])
    return self

  def setByWHXYArr(self, arr):
    self.setByXYWH(arr[2], arr[3], arr[0], arr[1])
    return self

  def setByCenterXYAndWH(self, x, y, w, h):
    self.offsetX = x - (0.5 * w)
    self.offsetY = y - (0.5 * h)
    self.width = w
    self.height = h
    return self

  def from_s(self, s):
    toks = map(lambda v: int(v), re.split(r"[+x]", s.split("_").pop()))
    self.setByWHXYArr(toks)
    return self

  def from_hs(self, hs):
    self.data = copy.deepcopy(hs)
    self.setByXYWH(hs["offsetX"], hs["offsetY"], hs["width"], hs["height"])
    del self.data["offsetX"], self.data["offsetY"], self.data["width"], self.data["height"]
    return self

  def to_s(self):
    s = "%04dx%04d+%04d+%04d" % (self.width, self.height, self.offsetX, self.offsetY)
    if self.data != None and "rot" in self.data.keys():
      s = "%s_rot%f" % (s, self.data["rot"])
    return s

  def getQuantized(self):
    qcr = Rect()
    qcr.setByXYWH(int(self.offsetX), int(self.offsetY), int(self.width), int(self.height))
    qcr.data = copy.deepcopy( self.data )
    return qcr

  def hasOverlapWith(self, another):
    if self.w() + another.w() < max(self.x2(), another.x2()) - min(self.x1(), another.x1()):
      return False
    elif self.h() + another.h() < max(self.y2(), another.y2()) - min(self.y1(), another.y1()):
      return False
    else:
      return True

  def getOverlapWith(self, another):
    if not self.hasOverlapWith(another):
      return None
    xs = [self.x1(), self.x2(), another.x1(), another.x2()]
    ys = [self.y1(), self.y2(), another.y1(), another.y2()]
    xs.sort()
    ys.sort()
    return Rect().setByX1Y1X2Y2( xs[1], ys[1], xs[2], ys[2] )

  def includesCenterOf(self, another):
    if another.x2() < self.x1() or self.x2() < another.x1() or another.y2() < self.y1() or self.y2() < another.y1():
      return False
    else:
      return True

  def covers(self, another):
    if another.x1() < self.x1() or self.x2() < another.x2() or another.y1() < self.y1() or self.y2() < another.y2():
      return False
    else:
      return True

  def getSupersetWith(self, another):
    xs = [self.x1(), self.x2(), another.x1(), another.x2()]
    ys = [self.y1(), self.y2(), another.y1(), another.y2()]
    xs.sort()
    ys.sort()
    return Rect.setByX1Y1X2Y2( xs[0], ys[0], xs[3], ys[4] ) 

  def extendToCover(self, another):
    self.setByX1Y1X2Y2( min(self.x1(), another.x1()), \
                        min(self.y1(), another.y1()), \
                        max(self.x2(), another.x2()), \
                        max(self.y2(), another.y2()) )
    return self

  def distant2From(self, another):
    r2  = ( self.averageX() - another.averageX() ) ** 2
    r2 += ( self.averageY() - another.averageY() ) ** 2
    return r2

  def distantFrom(self, another):
    return math.sqrt(self.distant2From(another))

  def resize(self, f):
    r = Rect().setByXYWH( self.x1() * f, self.y1() * f, self.w() * f, self.h() * f)
    r.data = copy.deepcopy(self.data)
    return r

  def resized(self, f):
    self.offsetX = self.offsetX * f
    self.offsetY = self.offsetY * f
    self.width   = self.width * f
    self.height  = self.height * f
    return self
