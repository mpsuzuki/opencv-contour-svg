#!/usr/bin/env ruby1.9
Opts = { "def-num-rect" => 20, "args" => [] }

require "json"
require "getOpts.rb"
require "libRect.rb"


def appendJson(dHash, fh)
  JSON.parse(fh.read)["jsonCropAreas"].each do |hs|
    b = hs["basename"]
    dHash[b] = Array.new if (!dHash.include?(b))
    dHash[b] << Rect.new.from_hs(hs.dup)
  end
end

rectsPerPage = Hash.new
if (Opts.args.length > 0)
  Opts.args.each do |pathJson|
    fh = File::open(pathJson, "r")
    appendJson(rectsPerPage, fh)
    fh.close
  end
else
  appendJson(rectsPerPage, STDIN)
end

ol = 0
jsonCropAreas = Array.new
rectsPerPage.keys.sort.each do |b|
  rects = rectsPerPage[b].dup.sort_by{|r| r.y1}

  vertLines = []
  while (cur = rects.shift)
    prev = vertLines.flatten.select{|r| cur != r && r.y1 < cur.y1 && cur.includesX(r.averageX)}.sort_by{|r| cur.distant2From(r)}.first
    if (prev == nil) 
      vertLines << [cur]
    else
      l = vertLines.select{|l| l.include?(prev)}.first
      if (l == nil)
        vertLines << [cur]
      else
        l << cur
      end
    end
  end

  vertLines.each do |l|
    l = l.sort_by{|r| r.y1}
  end
  vertLines = vertLines.sort_by{|l| l.first.x1}

  if (Opts.include?("dump-json-with-layout-info"))
    vertLines.each_with_index do |l, i|
      l.each_with_index do |hs, j|
        jsonCropAreas << {
          "basename" => b,
          "xIdx" => i,
          "yIdx" => sprintf("%02d", j),
          "offsetX" => hs.offsetX,
          "offsetY" => hs.offsetY,
          "width" => hs.width,
          "height" => hs.height
        }
      end
    end
  elsif (Opts.include?("debug"))
    puts ""
    puts b
    vertLines.each_with_index do |l, i|
      printf("  line #%d/%d (rects %d): ", i, vertLines.length, l.length)
      puts l.collect{|r| r.to_s.gsub(/^0*/, "").gsub(/x0*/, "x").gsub(/\+0*/, "+")}.join(", ")
    end
  elsif (Opts.include?("quiet") && vertLines.length == 5 && vertLines.all?{|l| l.length == Opts.def_num_rect})
    next
  elsif (Opts.include?("only-basename"))
    puts b
  else
    printf("% 3d: page %s: %d lines", ol, b, vertLines.length)
    puts "[" + vertLines.collect{|l| l.length == Opts.def_num_rect ? "--" : sprintf("%02d", l.length)}.join(", ")+ "]"
    ol += 1
  end
end

if (Opts.include?("dump-json-with-layout-info"))
  puts JSON.generate({"jsonCropAreas" => jsonCropAreas}, {
    :object_nl => "",
    :array_nl => "\n",
    :space => " ",
    :indent => " "
  }).gsub(/\{ +/, "{ ").gsub(/ +\}/, " }").gsub(/, +/, ", ")
end
