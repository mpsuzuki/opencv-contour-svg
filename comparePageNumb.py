#!/usr/bin/env python

import re
import sys
import os.path
import textwrap

import cv
import cv2
import base64

import numpy

import mygetopts
Opts = mygetopts.getOpts({}, sys.argv, [])

import libRect


def cmpImgsBySize(imgA, imgB):
  sizeA = imgA.shape[0] * imgA.shape[1]
  sizeB = imgB.shape[0] * imgB.shape[1]
  if sizeA == sizeB:
    return 0
  elif sizeA < sizeB:
    return 1
  else:
    return -1

img0 = cv2.imread( Opts["args"][0] )
img1 = cv2.imread( Opts["args"][1] )
imgs = [ img0, img1 ]
imgs.sort(cmpImgsBySize)

imgSmall = imgs[0]
imgBig = imgs[1]

# sizeBigger = imgBig.shape[0] + (2* imgSmall.shape[0]), imgBig.shape[1] + (2 * imgSmall.shape[1]), imgBig.shape[2]
sizeBigger = (2* imgBig.shape[0]) + imgSmall.shape[0], (2 * imgBig.shape[1]) + imgSmall.shape[1], imgBig.shape[2]
imgBigger = numpy.zeros(sizeBigger, imgBig.dtype)

if imgBigger.dtype == numpy.uint8:
  imgBigger.fill(255)

for iy in range(0, imgSmall.shape[0]):
  for ix in range(0, imgSmall.shape[1]):
    imgBigger[imgBig.shape[0] + iy][imgBig.shape[1] + ix] = imgSmall[iy][ix]

if "dump-big-image-on" in Opts.keys():
  cv2.imwrite(Opts["dump-big-image-on"], imgBigger)

#result = cv2.matchTemplate(imgBigger, imgSmall, cv2.TM_CCOEFF_NORMED)
result = cv2.matchTemplate(imgBigger, imgBig, cv2.TM_CCOEFF_NORMED)
def cmpByResult(a,b):
  if result[a[1], a[0]] == result[b[1], b[0]]:
    return 0
  elif result[a[1], a[0]] > result[b[1], b[0]]:
    return -1
  else:
    return 1

bbox_lls = []
locs = numpy.where( result >= float(Opts["threshold"]) )
for bbox_ll in zip(*locs[::-1]):
  bbox_lls.append(bbox_ll)
bbox_lls.sort(cmpByResult)

bbox_ll = bbox_lls[0]
if img0.shape == imgs[0].shape:
  print "%s %s" % (os.path.basename(Opts["args"][0]), os.path.basename(Opts["args"][1])),
else:
  print "%s %s" % (os.path.basename(Opts["args"][1]), os.path.basename(Opts["args"][0])),

print "%f (%d,%d)" % (result[bbox_ll[1], bbox_ll[0]], \
                        bbox_ll[0] - imgSmall.shape[1], \
                        bbox_ll[1] - imgSmall.shape[0])
