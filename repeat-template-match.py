#!/usr/bin/env python

import re
import sys
import os.path
import textwrap

import cv
import cv2
import base64

import numpy

import mygetopts
Opts = mygetopts.getOpts({}, sys.argv, [])

import libRect

 
bigImagePathName = Opts["args"][0]
bigImage = cv2.imread(bigImagePathName)
svgData  = "<svg width=\"%d\" height=\"%d\"" % ( bigImage.shape[1], bigImage.shape[0] )

svgData += " xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n"

svgData += "  <image x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"" % ( bigImage.shape[1], bigImage.shape[0] )

svgData += " basename=\"%s\"" % bigImagePathName

svgData += " xlink:href=\"%s\"" % bigImagePathName.replace("-blur/", "/")


svgData += " />\n"

for templateImagePathName in Opts["args"][1:]:
  geometryToFocus = None
  focusHeight, focusWidth = bigImage.shape[:2]
  focusOffsetX = 0
  focusOffsetY = 0
  bigImageFocused = bigImage

  if "@" in templateImagePathName:
    toks = templateImagePathName.split("@")
    templateImagePathName = toks[0]
    geometryToFocus = toks[1]
    toks = re.split("[+x]", geometryToFocus)
    focusWidth = int(toks[0])
    focusHeight = int(toks[1])
    focusOffsetX = int(toks[2])
    focusOffsetY = int(toks[3])
    bigImageFocused = bigImage[focusOffsetY:(focusOffsetY + focusHeight), focusOffsetX:(focusOffsetX + focusWidth)].copy()

  if geometryToFocus == None:
    sys.stderr.write("lookup %s..." % templateImagePathName)
  else:
    # sys.stderr.write("bigImageFocused %d x %d, type = %d\n" % (bigImageFocused.shape[0], bigImageFocused.shape[1], bigImageFocused.shape[2]))
    sys.stderr.write("lookup %s from %04dx%04d+%04d+%04d..." % ( templateImagePathName, focusWidth, focusHeight, focusOffsetX, focusOffsetY ))

  template = cv2.imread(templateImagePathName)

  templateHeight, templateWidth = template.shape[:2]

  exceedW = 0
  exceedH = 0
  if (focusWidth < templateWidth) or (focusHeight < templateHeight):
    # exceedW = templateWidth - focusWidth
    # exceedH = templateHeight - focusHeight
    exceedW = 20
    exceedH = 20
    sys.stderr.write("exceeds w=%d, h=%d, shrink to %dx%d..." % (exceedW, exceedH, templateWidth - exceedW, templateHeight - exceedH))
    template = template[exceedH:(templateHeight - exceedH), exceedW:(templateWidth - exceedW)]
    sys.stderr.write("shrinked w=%d, h=%d..." % (template.shape[1], template.shape[0]))

  # result = cv2.matchTemplate(bigImageFocused, template, cv.CV_TM_SQDIFF)
  result = cv2.matchTemplate(bigImageFocused, template, cv2.TM_CCOEFF_NORMED)
  # result = cv2.matchTemplate(bigImageFocused, template, cv2.TM_CCORR)
  # result = cv2.matchTemplate(bigImageFocused, template, cv2.TM_CCORR_NORMED)

  def cmp(a, b):
    if result[a[1], a[0]] == result[b[1], b[0]]:
      return 0
    elif result[a[1], a[0]] > result[b[1], b[0]]:
      return -1
    else:
      return 1

  bbox_lls = [ cv2.minMaxLoc(result)[2] ]
  if "threshold" in Opts.keys():
    sys.stderr.write("\n")
    bbox_lls.pop()
    locs = numpy.where( result >= float(Opts["threshold"]) )
    for bbox_ll in zip(*locs[::-1]):
      bbox_lls.append(bbox_ll)
    bbox_lls.sort(cmp)

  drawnRects = []
  for pt in bbox_lls:
    bbox_ll = (pt[0] - exceedW, pt[1] - exceedH)
    bbox_ru = (bbox_ll[0] + templateWidth, bbox_ll[1] + templateHeight)

    if geometryToFocus != None:
      bbox_ll = (bbox_ll[0] + focusOffsetX, bbox_ll[1] + focusOffsetY)
      bbox_ru = (bbox_ru[0] + focusOffsetX, bbox_ru[1] + focusOffsetY)

    rect = libRect.Rect().setByX1Y1X2Y2(bbox_ll[0], bbox_ll[1], bbox_ru[0], bbox_ru[1])
    rect.data["match"] = result[pt[1], pt[0]]
    if any(rect.includesCenterOf(rectD) for rectD in drawnRects):
      continue

    drawnRects.append( rect )

    if "threshold" in Opts.keys():
      sys.stderr.write("\t%f @ " % result[pt[1], pt[0]])

    sys.stderr.write("%dx%d+%d+%d\n" % (bbox_ru[0] - bbox_ll[0], bbox_ru[1] - bbox_ll[1], bbox_ll[0], bbox_ll[1]))

    svgData += "  <image opacity=\"0.5\""

    svgData += " x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"" % (bbox_ll[0], bbox_ll[1], bbox_ru[0] - bbox_ll[0], bbox_ru[1] - bbox_ll[1])

    imgSrc = templateImagePathName.replace("-blur/", "/").replace("pagePNG", "pageJPG")
    svgData += " id=\"%s\" xlink:href=\"%s\" />\n" % ( os.path.basename(templateImagePathName), imgSrc)

    svgData += "  <rect fill-opacity=\"0.2\" fill=\"%s\" stroke-opacity=\"0.75\" stroke=\"%s\" stroke-width=\"1\"" % ( "magenta", "red" )
    svgData += " x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"" % (bbox_ll[0], bbox_ll[1], bbox_ru[0] - bbox_ll[0], bbox_ru[1] - bbox_ll[1])
    if "threshold" in Opts.keys():
      svgData += " threshold=\"%f\"" % result[pt[1], pt[0]]
    svgData += " id=\"%s\"/>\n" % os.path.basename(templateImagePathName)

  del template
  if geometryToFocus != None:
    del bigImageFocused

del bigImage
 
svgData += "</svg>\n"

sys.stdout.write(svgData)
