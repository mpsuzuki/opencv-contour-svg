#!/usr/bin/env python

import sys
import os.path

import numpy
import cv2

import re
Opts = {}
Opts["circle"] = False
Opts["args"] = []
Opts["blur-x"] = 0
Opts["blur-y"] = 0
Opts["page-number-height"] = "0..0"
Opts["glyph-height"] = "0..0"
Opts["hint-rect"] = []

import mygetopts
Opts = mygetopts.getOpts(Opts, sys.argv, ["circle"])

from xml.dom import minidom
import json
jsDoc = minidom.parseString("<opts><!-- " + json.dumps(Opts) + " --></opts>")

Opts["pathImg"] = Opts["args"].pop(0)

for k in [ "blur-x", "blur-y" ]:
  Opts[k] = 1 + 2 * int(Opts[k])

for k in [ "page-number-height", "glyph-height" ]:
  Opts[k] = Opts[k].split("..")
  Opts[k] = range(int(Opts[k][0]), int(Opts[k][1]))

for k in [ "page-number-width", "glyph-width" ]:
  if k in Opts.keys():
    Opts[k] = Opts[k].split("..")
    Opts[k] = range(int(Opts[k][0]), int(Opts[k][1]))

if "hint-svg" in Opts.keys() and os.path.isfile(Opts["hint-svg"]):
  Opts["hint-rect"] = filter(lambda elm: elm.getAttribute("class").find("corrected") >= 0, \
                             minidom.parse(Opts["hint-svg"]).documentElement.getElementsByTagName("rect"))

img = cv2.imread(Opts["pathImg"])
for elmHint in Opts["hint-rect"]:
  x1 = int(elmHint.getAttribute("x"))
  y1 = int(elmHint.getAttribute("y"))
  x2 = x1 + int(elmHint.getAttribute("width"))
  y2 = y1 + int(elmHint.getAttribute("height"))
  cv2.rectangle(img, (x1, y1), (x2, y2), (255,255,255), -1)

hImg, wImg = img.shape[:2]
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

if Opts["blur-x"] > 1 or Opts["blur-y"] > 1:
  kernel = numpy.ones((Opts["blur-y"], Opts["blur-x"]), numpy.float32)/(Opts["blur-x"] * Opts["blur-y"])
  imgGrayInversed = cv2.filter2D((255 - imgGray), -1, kernel)
else:
  imgGrayInversed = (255 - imgGray)

contoursRaw, hier = cv2.findContours( imgGrayInversed.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# contours = [cv2.approxPolyDP(aCnt, 3, True) for aCnt in contoursRaw]

svgSkel = """
<?xml-stylesheet type="text/css" href="ywzs.css" ?>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:html="http://www.w3.org/1999/xhtml">
</svg>
"""

svgDoc = minidom.parseString(svgSkel)
svgRoot = svgDoc.documentElement
svgRoot.setAttribute("width", str(wImg))
svgRoot.setAttribute("height", str(hImg))

svgRoot.appendChild(jsDoc.documentElement.childNodes[0])

elmImg = svgDoc.createElement("image")
svgRoot.appendChild(elmImg)
elmImg.setAttribute("width", str(wImg))
elmImg.setAttribute("height", str(hImg))
elmImg.setAttribute("xlink:href", Opts["pathImg"])

for idx, aCnt in enumerate(contoursRaw):
  if hier[0][idx][3] != -1:
    continue

  if Opts["circle"]:
    (cntX, cntY), cntRad = cv2.minEnclosingCircle(aCnt)
    elmCont = svgDoc.createElement("circle")
    elmCont.setAttribute("cx", str(cntX))
    elmCont.setAttribute("cy", str(cntY))
    elmCont.setAttribute("r", str(cntRad))

  else:
    rectX, rectY, rectW, rectH = cv2.boundingRect(aCnt)
    elmCont = svgDoc.createElement("rect")
    elmCont.setAttribute("x", str(rectX))
    elmCont.setAttribute("y", str(rectY))
    elmCont.setAttribute("width", str(rectW))
    elmCont.setAttribute("height", str(rectH))
    if rectH in Opts["page-number-height"] and ("page-number-width" not in Opts.keys() or rectW in Opts["page-number-width"]):
      elmCont.setAttribute("class", "page-number")
    elif rectH in Opts["glyph-height"] and ("glyph-width" not in Opts.keys() or rectW in Opts["glyph-width"]):
      elmCont.setAttribute("class", "glyph")
    elif rectW > (2 * max(Opts["glyph-width"])):
      elmCont.setAttribute("class", "line")


  #if elmCont.getAttribute("fill") == "":
  #  elmCont.setAttribute("fill-opacity", "0.2")
  #  elmCont.setAttribute("stroke-opacity", "0.75")
  #  elmCont.setAttribute("fill", "magenta")
  #  elmCont.setAttribute("stroke", "red")

  svgRoot.appendChild(elmCont)

for elmHint in Opts["hint-rect"]:
  svgRoot.appendChild(elmHint)

elmLines = filter(lambda elm: elm.getAttribute("class").find("line") >= 0, svgDoc.getElementsByTagName("rect"))
elmGlyphs = filter(lambda elm: elm.getAttribute("class").find("glyph") >= 0, svgDoc.getElementsByTagName("rect"))
elmPageNumbers = filter(lambda elm: elm.getAttribute("class").find("page-numbers") >= 0, svgDoc.getElementsByTagName("rect"))
elmBits = filter(lambda elm: \
                   elm.getAttribute("class").find("line") < 0 and \
                   elm.getAttribute("class").find("glyph") < 0 and \
                   elm.getAttribute("class").find("page-numbers") < 0,
                 svgDoc.getElementsByTagName("rect"))

for e in elmLines:
  svgRoot.appendChild(e)
for e in elmGlyphs:
  svgRoot.appendChild(e)
for e in elmPageNumbers:
  svgRoot.appendChild(e)
for e in elmBits:
  svgRoot.appendChild(e)

#print(svgDoc.toxml())
print(svgDoc.toprettyxml())
