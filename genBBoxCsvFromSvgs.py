#!/usr/bin/env python

import sys
import os.path
from collections import Counter
from xml.dom import minidom

import mygetopts
Opts = mygetopts.getOpts({}, sys.argv, ["height"])
if "unit" in Opts.keys():
  Opts["unit"] = int(Opts["unit"])
for k in ["height-range", "width-range"]:
  if k in Opts.keys():
    Opts[k] = Opts[k].split("..", 2)
    Opts[k] = range( int(Opts[k][0]), int(Opts[k][1]) )
  else:
    Opts[k] = None

countWidth = Counter()
countHeight = Counter()

for pathSvg in Opts["args"]:
  svgDoc = minidom.parse(pathSvg)
  svgRoot = svgDoc.documentElement
  elmRects = svgRoot.getElementsByTagName("rect")
  for elmRect in elmRects:
    w = elmRect.getAttribute("width")
    h = elmRect.getAttribute("height")
    if "unit" in Opts.keys():
      w = str(int(round(float(w) / Opts["unit"]) * Opts["unit"]))
      h = str(int(round(float(h) / Opts["unit"]) * Opts["unit"]))


    if Opts["height-range"] and not h in Opts["height-range"]:
      continue
    elif Opts["width-range"] and not w in Opts["width-range"]:
      continue
    else:
      countWidth[w] += 1
      countHeight[h] += 1

if "width" in Opts.keys():
  for w, cnt in countWidth.most_common():
    print ", ".join([w, str(cnt)])
else:
  for h, cnt in countHeight.most_common():
    print ", ".join([h, str(cnt)])
